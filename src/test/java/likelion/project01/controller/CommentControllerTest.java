package likelion.project01.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import likelion.project01.domain.dto.comment.CommentCreateRequest;
import likelion.project01.domain.dto.comment.CommentCreateResponse;
import likelion.project01.domain.dto.post.PostCreateRequest;
import likelion.project01.domain.dto.post.PostDto;
import likelion.project01.exception.AppException;
import likelion.project01.exception.ErrorCode;
import likelion.project01.service.CommentService;
import likelion.project01.service.HelloService;
import likelion.project01.service.PostService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CommentController.class)
class CommentControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    CommentService commentService;

    @MockBean
    PostService postService;

    String userName = "jihwan";
    Long postId = 1l;
    CommentCreateRequest commentCreateRequest = CommentCreateRequest.builder()
            .comment("1빠")
            .build();

    @Test
    @WithMockUser
    @DisplayName("댓글 작성")
    void create_comment() throws Exception {

        when(commentService.createComment(userName, postId, commentCreateRequest))
                .thenReturn(CommentCreateResponse.builder()
                        .comment("1빠")
                        .build());

        mockMvc.perform(post("/comments/1/comment")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentCreateRequest)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 작성 실패(1) - 로그인 하지 않은 경우")
    void faile_create_comment01() throws Exception {

        String guest = "jj";

        when(commentService.createComment(guest, postId, commentCreateRequest))
                .thenThrow(new AppException(ErrorCode.USERNAME_NOT_FOUND, "로그인을 하지 않았습니다."));

        mockMvc.perform(post("/comments/1/comment")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentCreateRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized()) // 401
                .andExpect(jsonPath("$.result.message").value("로그인을 하지 않았습니다."));
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 작성 실패(2) - 게시물이 존재하지 않는 경우")
    void faile_create_comment02() throws Exception {

        when(commentService.createComment(userName, postId, commentCreateRequest))
                .thenThrow(new AppException(ErrorCode.POST_NOT_FOUND, "게시글이 존재하지 않습니다."));

        mockMvc.perform(post("/comments/1/comment")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentCreateRequest)))
                .andDo(print())
                .andExpect(status().isNotFound()) // 401
                .andExpect(jsonPath("$.result.message").value("로그인을 하지 않았습니다."));
    }


}