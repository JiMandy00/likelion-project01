package likelion.project01.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import likelion.project01.domain.dto.post.PostCreateRequest;
import likelion.project01.domain.dto.post.PostDto;
import likelion.project01.domain.dto.post.PostEditRequest;
import likelion.project01.domain.entity.Post;
import likelion.project01.exception.AppException;
import likelion.project01.exception.ErrorCode;
import likelion.project01.repository.PostRepository;
import likelion.project01.service.PostService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PostController.class)
class PostControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    PostService postservice;

    @Autowired
    ObjectMapper objectMapper;

    private final Long postId = 1l;

    private final PostRepository postRepository;

    PostControllerTest(PostRepository postRepository) {
        this.postRepository = postRepository;
    }




    @Test
    @DisplayName("글 작성하기 - 성공")
    @WithMockUser
    void createPost() throws Exception {
        PostCreateRequest postCreateRequest = PostCreateRequest.builder()
                .title("크리스마스엔 든든한 국밥을 먹어야지")
                .content("부산 광역시 합천 돼지국밥 웨이팅 레전드")
                .build();

        when(postservice.createPost(any(),any())).thenReturn(mock(PostDto.class));

        mockMvc.perform(post("/api/v1/post/createPost")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postCreateRequest)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("글 작성하기 - 실패 : 인증 실패")
    @WithMockUser
    void failedCreatePost01() throws Exception {
        PostCreateRequest postCreateRequest = PostCreateRequest.builder()
                .title("크리스마스엔 든든한 국밥을 먹어야지")
                .content("부산 광역시 합천 돼지국밥 웨이팅 레전드")
                .build();

        when(postservice.createPost(any(),any())).thenThrow(new AppException(ErrorCode.INVALID_PERMISSION, "권한이 없습니다."));

        mockMvc.perform(post("/api/v1/post/createPost")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postCreateRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.result.message").value("권한이 없습니다."));
    }

    @Test
    @DisplayName("글 작성하기 - 실패 : 토큰 기한이 만료됨")
    @WithMockUser
    void failedCreatePost02() throws Exception {
        PostCreateRequest postCreateRequest = PostCreateRequest.builder()
                .title("크리스마스엔 든든한 국밥을 먹어야지")
                .content("부산 광역시 합천 돼지국밥 웨이팅 레전드")
                .build();

        when(postservice.createPost(any(),any())).thenThrow(new AppException(ErrorCode.INVALID_TOKEN, "유효한 토큰이 아닙니다."));

        mockMvc.perform(post("/api/v1/post/createPost")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postCreateRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.result.message").value("유효한 토큰이 아닙니다."));
    }

    @Test
    @DisplayName("글 수정하기 - 완료")
    @WithMockUser
    void editPost() throws Exception {

        Post post = Post.builder()
                .title("글을 작성합니다.")
                .content("작성 됐나여?")
                .build();

        postRepository.save(post);

        PostEditRequest postEditRequest = PostEditRequest.builder()
                .title("글을 수정 해보겠다.")
                .content("수정 됐나여?")
                .build();

        when(postservice.edit(any(), any(),any())).thenThrow(new AppException(ErrorCode.INVALID_PERMISSION, "수정 권한이 없습니다."));

        mockMvc.perform(put("/api/v1/post/editPost/{id}", postId)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postEditRequest)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.postId").value(postId))
                .andExpect(jsonPath("$.result.message").value("포스트 수정이 완료 되었습니다."))
                .andDo(print())
                ;
    }
}