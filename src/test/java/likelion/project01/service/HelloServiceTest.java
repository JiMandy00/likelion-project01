package likelion.project01.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import likelion.project01.controller.CommentController;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// annotation없이 만들기
@WebMvcTest(HelloServiceTest.class)
class HelloServiceTest {

    // spring을 쓰지 않고 테스트를 하기 때문에 new를 이용해서 초기화
    // pojo 방식을 최대한 활용
    HelloService helloService = new HelloService();

    @Test
    @DisplayName("자릿수 합 잘 구하는지")
    void sumOdDigit() {
        assertEquals(6, helloService.col(123));
        assertEquals(9, helloService.col(234));
        assertEquals(12, helloService.col(345));
    }


}