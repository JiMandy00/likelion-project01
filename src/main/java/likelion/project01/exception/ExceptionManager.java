package likelion.project01.exception;

import likelion.project01.domain.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionManager {

    @ExceptionHandler(AppException.class)
    public ResponseEntity<?> exceptionHandler(AppException e) {
        ErrorResponse errorResponse = new ErrorResponse(e.getErrorCode(),e.getMessage());
        return ResponseEntity.status(e.getErrorCode().getHttpStatus())
                .body(Response.error("ERROR", errorResponse));
    }




//    @ExceptionHandler(AppException.class)
//    public ResponseEntity<?> appExceptionHandler(AppException e) {
//        return ResponseEntity.status(e.getErrorCode().getHttpStatus())
//                .body(e.getErrorCode().name() + " " + e.getMessage());
//    }
//
//    @ExceptionHandler(RuntimeException.class)
//    public ResponseEntity<?> runtimeExceptionHandler(RuntimeException e) {
//        return ResponseEntity.status(HttpStatus.CONFLICT)
//                .body(e.getMessage());
//    }
}
