package likelion.project01.domain.entity;

import lombok.*;
import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Alarm extends BaseEntity{
    // 알람 id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 알람 타입
    @Column(name = "alram_type")
    @Enumerated(EnumType.STRING) // alramType을 String으로 DB에 저장
    private AlarmType alarmType;

    // 알람을 받는 유저
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    // 알람을 주는(?) 유저
    @Column(name = "from_user_id")
    private Long fromUserId;

    // 알람이 생성된 게시글
    @Column(name = "targer_id")
    private Long targetId;

    // 알람 내용
    private String text;
}
