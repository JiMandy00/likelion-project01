package likelion.project01.domain.entity;

import likelion.project01.domain.dto.comment.CommentDto;
import likelion.project01.domain.dto.comment.CommentUpdateResponse;
import likelion.project01.domain.entity.Post;
import likelion.project01.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Comment extends BaseEntity{
    @Id
    @GeneratedValue
    @Column(name = "comment_id")
    private Long id;

    // 제목(내용)
    @Column(length = 100)
    private String comment;

    // 글쓴이
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    // 포스트 id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    private Post postId;

    public CommentDto toDto() {
        return CommentDto.builder()
                .id(this.id)
                .comment(this.comment)
                .userName(this.getUser().getUserName())
                .postId(this.getPostId().getId())
                .createdAt(this.getCreatedAt())
                .build();
    }

    public void update(String comment) {
        this.comment = comment;
    }

    public CommentUpdateResponse commentUpdateResponse(Comment comment) {
        return CommentUpdateResponse.builder()
                .id(comment.getId())
                .comment(comment.getComment())
                .userName(comment.getUser().getUserName())
                .postId(comment.getPostId().getId())
                .createdAt(comment.getCreatedAt())
                .lastModifiedAt(comment.getLastModifiedAt())
                .build();
    }
}
