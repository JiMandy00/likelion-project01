package likelion.project01.domain.entity;

import lombok.Getter;

@Getter
public enum AlarmType {
    NEW_COMMENT_ON_POST,
    NEW_LIKE_ON_POST
}
