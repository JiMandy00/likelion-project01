package likelion.project01.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import likelion.project01.domain.dto.post.PostDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Post extends BaseEntity {
    @Id
    @GeneratedValue
    @Column(name = "post_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore // 직렬화 속성 값 무시
    private User user;

    private String title;
    private String content;

    @OneToMany
    // map으로 할 수 있을까?
    private List<Likes> countLikes = new ArrayList<>();

    public Post toDto() {
        return Post.builder()
                .id(this.id)
                .user(this.user)
                .title(this.title)
                .content(this.content)
                .build();
    }

    public void update(String title, String content) {
        this.title = title;
        this.content = content;
    }
}
