package likelion.project01.domain.dto.post;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostEditResponse {
    private Long postId;
    private String message; // 완료 됐다는 message
}
