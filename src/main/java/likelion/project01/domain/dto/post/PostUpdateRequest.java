package likelion.project01.domain.dto.post;

import likelion.project01.domain.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PostUpdateRequest {
    private Long postId;
    private String title;
    private String content;

    // update에 사용 하려고 일단 만듦..
    public Post toEntity() {
        return Post.builder()
                .title(this.title)
                .content(this.content)
                .build();
    }
}
