package likelion.project01.domain.dto.post;


import likelion.project01.domain.entity.Post;
import likelion.project01.domain.entity.User;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
public class PostCreateRequest {
    private String title;
    private String content;
}
