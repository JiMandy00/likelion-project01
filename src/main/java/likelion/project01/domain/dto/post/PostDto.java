package likelion.project01.domain.dto.post;

import likelion.project01.domain.entity.Post;
import likelion.project01.domain.entity.User;
import lombok.*;
import net.bytebuddy.asm.Advice;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostDto {
    private Long id;
    private User user;
    private String title;
    private String content;
    private LocalDateTime createdAt;
    private LocalDateTime lastModifiedAt;

//    public static Page<PostDto> postDto(Page<Post> posts) {
//        Page<PostDto> postDtos = posts.map(post -> PostDto.builder()
//                .id(posts.getId())
//                .title(posts.getTitle())
//                .title(posts.getContent())
//                .user(posts.getUser())
//                .createAt(posts.getCreateAt())
//                .lastModifiedAt(posts.getLastModifiedAt())
//                .build());
//        return postDtos;
}

