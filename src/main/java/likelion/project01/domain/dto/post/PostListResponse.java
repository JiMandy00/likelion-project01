package likelion.project01.domain.dto.post;

import likelion.project01.domain.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PostListResponse {
    private Page<Post> posts;
}
