package likelion.project01.domain.dto.post;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PostCreateResponse {
    private Long postId;
    private String message; // 완료 됐다는 message
}
