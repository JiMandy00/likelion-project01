package likelion.project01.domain.dto.post;

import likelion.project01.domain.entity.Post;
import lombok.*;
import org.springframework.data.domain.Page;

import java.time.format.DateTimeFormatter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class PostDetailResponse {
    private Long id;
    private String title;
    private String content;
    private String userName;
    private String createAt;
    private String lastModifiedAt;

    public static Page<PostDetailResponse> toList(Page<Post> post) {
        Page<PostDetailResponse> postDetailResponses = post.map(m -> PostDetailResponse.builder()
                .id(m.getId())
                .title(m.getTitle())
                .content(m.getContent())
                .userName(m.getUser().getUserName())
                .createAt(m.getCreatedAt().format(DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm:ss")))
                .lastModifiedAt(m.getLastModifiedAt().format(DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm:ss")))
                .build());

        return postDetailResponses;
    }

}
