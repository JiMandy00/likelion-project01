package likelion.project01.domain.dto.Alram;

import com.fasterxml.jackson.annotation.JsonFormat;
import likelion.project01.domain.entity.AlarmType;
import likelion.project01.domain.entity.Alarm;
import likelion.project01.domain.entity.User;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AlarmDto {
    private Long id;

    private AlarmType alarmType;

    // 알람을 받을 id
    private User userId;

    // 알람을 보낸 id
    private Long fromUserId;

    // 게시글 id
    private Long targetId;

    // 알람 내용
    private String text;

    // 알람 생성 일자
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime createAt;


    public static AlarmDto of(Alarm alarm) {
        return AlarmDto.builder()
                .id(alarm.getId())
                .alarmType(alarm.getAlarmType())
                .fromUserId(alarm.getFromUserId())
                .targetId(alarm.getTargetId())
                .text(alarm.getText())
//                .createAt()
                .build();
    }
}
