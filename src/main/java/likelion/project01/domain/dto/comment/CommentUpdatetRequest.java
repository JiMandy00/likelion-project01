package likelion.project01.domain.dto.comment;

import likelion.project01.domain.entity.Comment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class CommentUpdatetRequest {
    private String comment;

    public Comment toEntity() {
        return Comment.builder()
                .comment(this.comment)
                .build();
    }
}
