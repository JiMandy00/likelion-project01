package likelion.project01.domain.dto.comment;

import com.fasterxml.jackson.annotation.JsonFormat;
import likelion.project01.domain.entity.Comment;
import likelion.project01.domain.entity.Post;
import likelion.project01.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommentCreateRequest {
    private String comment;

    public Comment toEntity(User user, Post post) {
        return Comment.builder()
                .comment(this.comment)
                .user(user)
                .postId(post)
                .build();
    }
}
