package likelion.project01.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.ToString;

@ToString
public enum UserRole {
    ADMIN, USER;

    @JsonCreator
    public static UserRole from(String s) {
        return UserRole.valueOf(s.toUpperCase());
    }
}
