package likelion.project01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LikelionProject01Application {

    public static void main(String[] args) {
        SpringApplication.run(LikelionProject01Application.class, args);
    }

}
