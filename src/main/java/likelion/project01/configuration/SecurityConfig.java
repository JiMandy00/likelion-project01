package likelion.project01.configuration;

import likelion.project01.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class SecurityConfig {

    private UserService userService;

    @Value("${jwt.token.secret}")
    private String secretKey;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .httpBasic().disable() // ui
                .csrf().disable()// cross site
                .cors().and()// cross site에서 허용해준다.
                .authorizeRequests()
                .antMatchers("/api/v1/join", "/api/v1/login", "/api/v1/hello", "/api/v1/post/list", "/api/v1/post/detail/**").permitAll()
                .antMatchers("/api/v1/post/createPost", "/api/v1/post/editPost/**", "/api/v1/post/delete/**" ).permitAll()
//                .antMatchers("/api/v1/post/createPost", "/api/v1/post/editPost/**", "/api/v1/post/delete/**" ).hasAnyRole("ADMIN")
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS) // jwt 사용하는 경우
                .and()
                .addFilterBefore(new JwtFilter(userService, secretKey), UsernamePasswordAuthenticationFilter.class)
                .build();
    }
}
