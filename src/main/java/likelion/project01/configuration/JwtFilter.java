package likelion.project01.configuration;


import likelion.project01.service.UserService;
import likelion.project01.util.JwtUtil;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Slf4j
@RequiredArgsConstructor
@AllArgsConstructor
public class JwtFilter extends OncePerRequestFilter {

    private UserService userService;
    private String secretKey;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // token
        String token = request.getHeader(AUTHORIZATION);

        // token이 빈 값이면 권한을 주지 않음
        if (token == null || !token.startsWith("Bearer ")) {
            // filterchain 종료
            filterChain.doFilter(request, response);
            return;
        }


        String createdToken;

        // token split
        try {
            createdToken = token.split(" ")[1];
            // 만약 token에 예외가 발생하면 filterchain 종료
        } catch (Exception e) {
            log.error("Token이 없습니다.");
            filterChain.doFilter(request, response);
            return;
        }

        // token 만료 체크
        if (JwtUtil.isExpired(createdToken, secretKey)) {
            log.error("해당 token은 만료되었습니다.");
            // filterchain 종료
            filterChain.doFilter(request, response);
            return;
        }

        String userName = JwtUtil.getUserName(createdToken, secretKey);
        log.info("userName:{}", userName);


        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(userName, null, List.of(new SimpleGrantedAuthority("USER")));

        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        filterChain.doFilter(request, response);

    }
}
