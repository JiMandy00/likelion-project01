package likelion.project01.service;

import likelion.project01.domain.entity.Likes;
import likelion.project01.domain.entity.Post;
import likelion.project01.domain.entity.User;
import likelion.project01.exception.AppException;
import likelion.project01.exception.ErrorCode;
import likelion.project01.repository.LikeRepository;
import likelion.project01.repository.PostRepository;
import likelion.project01.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LikeService {

    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final LikeRepository likeRepository;

    // 1. 좋아요 누르기
    public String putLike(String userName, Long postId) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, "사용자를 찾을 수 없습니다."));

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, "게시글을 찾을 수 없습니다."));

        if (isNotAlreadyLike(user.getUserName(), post.getId())) {
            likeRepository.save(new Likes(post, user));
            return "게시글에 좋아요를 눌렀습니다.";
        } else {
            likeRepository.delete(new Likes(post, user));
            return "게시글에 좋아요를 취소했습니다.";
        }
    }

    // 2. 좋아요 개수
    public Integer countLike(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, "게시글을 찾을 수 없습니다."));
        int counting = post.getCountLikes().size();
        return counting;
    }

    // 3. 좋아요 중복을 확인하는 메서드
    private boolean isNotAlreadyLike(String userName, Long postId) {
        return likeRepository.findByUserAndPost(userName, postId).isEmpty();
    }
}
