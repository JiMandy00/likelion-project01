package likelion.project01.service;

import likelion.project01.domain.dto.post.PostDeleteResponse;
import likelion.project01.domain.dto.post.*;
import likelion.project01.domain.entity.Post;
import likelion.project01.domain.entity.User;
import likelion.project01.exception.AppException;
import likelion.project01.exception.ErrorCode;
import likelion.project01.repository.PostRepository;
import likelion.project01.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class PostService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final UserService userService;


    // 글 리스트 보기
    public Page<Post> postList(Pageable pageable) {
        return postRepository.findAll(pageable);
    }

    // 글 작성
    public PostDto createPost(PostCreateRequest request, String userName) {
        // 토큰으로 로그인 하였는지 확인
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, userName + "이 없습니다."));

        Post post = Post.builder()
                .title(request.getTitle())
                .content(request.getContent())
                .user(user)
                .build();



        Post savePost = postRepository.save(post.toDto());

        return PostDto.builder()
                .id(savePost.getId())
                .title(savePost.getTitle())
                .content(savePost.getContent())
                .build();
    }

    public Page<Post> findAll(Pageable pageable) {
        return postRepository.findAll(pageable);
    }

    // 게시글 수정
    @Transactional
    public Long edit(Long postId, PostUpdateRequest postUpdateRequest, String userName) {
        // user name을 repo에서 찾아서 eintiy에 대입
        // authentication된 userName...
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, userName + "이 없습니다."));

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, postId + "가 없습니다"));


        if (user.getUserName() == post.getUser().getUserName()) {
            // 수정

            post.update(postUpdateRequest.getTitle(), postUpdateRequest.getContent());
            postRepository.save(post);

            return postId;

        } else {
            throw new AppException(ErrorCode.INVALID_PERMISSION, "본인이 작성한 글이 아니면 수정할 수 없습니다.");
        }

    }

    // 게시글 삭제
    @Transactional
    public PostDeleteResponse delete(Long postId, String userName) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, userName + "이 없습니다."));

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, postId + "가 없습니다"));


        if (user.getUserName() == post.getUser().getUserName()) {
            // 삭제
            postRepository.delete(post);

            // response
            PostDeleteResponse postDeleteResponse = PostDeleteResponse.builder()
                    .postId(postId)
                    .message("게시글 삭제가 완료 되었습니다.")
                    .build();
            return postDeleteResponse;
        } else {
            throw new AppException(ErrorCode.INVALID_PERMISSION, "본인이 작성한 글이 아니면 삭제할 수 없습니다.");
        }

//        // 삭제할 권한이 있는지 확인
//        // stream - (filter) - map - collect
//        List<String> checkRole = authentication.getAuthorities().stream()
//                .map(GrantedAuthority::getAuthority)
//                .collect(Collectors.toList());
//        String userName = authentication.getName();
    }

    // 글 상세
    public PostDetailResponse detail(String userName, Long postId) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, "존재하지 않는 유저 이름입니다."));
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, "해당 포스트가 존재하지 않습니다."));

        PostDetailResponse postDetailResponse = PostDetailResponse.builder()
                .id(postId)
                .userName(post.getUser().getUserName())
                .title(post.getTitle())
                .content(post.getContent())
                .createAt(post.getCreatedAt().format(DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm:ss")))
                .lastModifiedAt(post.getLastModifiedAt().format(DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm:ss")))
                .build();
        return postDetailResponse;
    }

}
