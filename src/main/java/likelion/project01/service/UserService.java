package likelion.project01.service;

import likelion.project01.domain.dto.User.UserDto;
import likelion.project01.domain.dto.User.UserJoinRequest;
import likelion.project01.domain.entity.User;
import likelion.project01.exception.AppException;
import likelion.project01.exception.ErrorCode;
import likelion.project01.repository.UserRepository;
import likelion.project01.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;
    @Value("${jwt.token.secret}")
    private String key;
    private Long expireTimeMs = 1000 * 60 * 60l; // 1시간만 token 유효시간 부여


    public UserDto join(UserJoinRequest request) {
        // userName 중복체크
        userRepository.findByUserName(request.getUserName())
                .ifPresent(user -> {
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME, user + "는 이미 있습니다.");
                });

        // 중복이 아니면 db에 저장
        User user = userRepository.save(request.toEntity(encoder.encode(request.getPassword())));

        return UserDto.builder()
                .id(user.getId())
                .userName(user.getUserName())
                .role(user.getRole())
                .build();
    }

    public String login(String userName, String password) {
        // username 없음
        User selectedUser = userRepository.findByUserName(userName)
                .orElseThrow(() ->
                        new AppException(ErrorCode.USERNAME_NOT_FOUND, userName + "이 없습니다.")
                );

        // password 틀림
        if (!encoder.matches(password, selectedUser.getPassword())) {
            throw new AppException(ErrorCode.INVALID_PASSWORD, "패스워드를 잘못 입력 했습니다.");
        }

        String token = JwtTokenUtil.createToken(selectedUser.getUserName(), key, expireTimeMs);
        return token;
    }

    public User getUserByUserName(String userName) {
        Optional<User> user = userRepository.findByUserName(userName);
        User getUser = user.get();
        return getUser;
    }
}
