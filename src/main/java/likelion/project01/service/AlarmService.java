package likelion.project01.service;

import likelion.project01.domain.dto.Alram.AlarmDto;
import likelion.project01.domain.entity.Alarm;
import likelion.project01.domain.entity.User;
import likelion.project01.exception.AppException;
import likelion.project01.exception.ErrorCode;
import likelion.project01.repository.AlarmRepository;
import likelion.project01.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AlarmService {
    private final AlarmRepository alarmRepository;
    private final UserRepository userRepository;

    public Page<AlarmDto> alarmList(Pageable pageable, String userName) {
        // 토큰으로 로그인 하였는지 확인
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, "사용자를 찾을 수 없습니다."));
        // 알람 리스트 return
        return alarmRepository.findByUser(user, pageable).map(alarm -> AlarmDto.of(alarm));
    }
}
