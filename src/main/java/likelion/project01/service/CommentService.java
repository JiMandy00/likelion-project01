package likelion.project01.service;

import likelion.project01.domain.dto.comment.*;
import likelion.project01.domain.entity.Comment;
import likelion.project01.domain.entity.Post;
import likelion.project01.domain.entity.User;
import likelion.project01.exception.AppException;
import likelion.project01.exception.ErrorCode;
import likelion.project01.repository.CommentRepository;
import likelion.project01.repository.PostRepository;
import likelion.project01.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommentService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;

    // 1. 댓글 목록 조회
    public CommentListResponse commentList(Long postId, Pageable pageable) {
        Page<Comment> page = commentRepository.findByPostId(postId, pageable);
        List<CommentDto> list = new ArrayList<>();
        for (Comment comment : page) {
            CommentDto commentDto = CommentDto.builder()
                    .id(comment.getId())
                    .comment(comment.getComment())
                    .userName(comment.getUser().getUserName())
                    .postId(postId)
                    .createdAt(comment.getCreatedAt())
//                    .lastModifiedAt(comment.getLastModifiredAt())
                    .build();
            list.add(commentDto);
        }

        CommentListResponse commentListResponse = CommentListResponse.builder()
                .id(list)
                .pageable(pageable)
                .build();

        return commentListResponse;


    }

    // 2. 댓글 작성
    public CommentDto createComment(String userName, Long postId, CommentCreateRequest commentCreateRequest) {
        // 토큰으로 로그인 하였는지 확인
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, "사용자를 찾을 수 없습니다."));

        // 포스트가 존재하는지 확인
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, "게시글을 찾을 수 없습니다."));

        // 댓글을 작성할 포스트 id, 댓글 작성자 이름, 댓글 내용을 저장
        Comment comment = commentRepository.save(commentCreateRequest.toEntity(user, post));

        // 사용자에게 보여줄 댓글의 내용/정보
        CommentDto commentDto = comment.toDto();

        return commentDto;
    }

    // 3. 댓글 수정
    @Transactional
    public CommentUpdateResponse editComment(CommentUpdatetRequest commentUpdatetRequest, Long postId, Long commentId, String userName) {
        // 토큰으로 로그인 하였는지 확인
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, "사용자를 찾을 수 없습니다."));

        // 포스트가 존재하는지 확인
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, "게시글이 존재하지 않습니다."));

        // 댓글이 존재하는지 확인
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new AppException(ErrorCode.COMMENT_NOT_FOUND, "댓글이 존재하지 않습니다."));

        // 댓글 작성자와 수정을 요청한 사용자의 userName이 일치하는지 확인
        // 일치하면 => 수정
        // 일치하지 않으면 => error code
        if (user.getUserName() == comment.getUser().getUserName()) {
            comment.update(commentUpdatetRequest.getComment());
            Comment updatedComment = commentRepository.save(comment);
            log.info("수정 완료");
            return updatedComment.commentUpdateResponse(updatedComment);
        } else {
            throw new AppException(ErrorCode.INVALID_PERMISSION, "본인이 작성한 댓글이 아니면 수정할 수 없습니다.");
        }

    }


    // 4. 댓글 삭제
    @Transactional
    public CommentDeleteResponse deleteComment(String userName, Long commentId, Long postId) {
        // 토큰으로 로그인 하였는지 확인
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, "사용자를 찾을 수 없습니다."));

        // 포스트가 존재하는지 확인
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, "게시글이 존재하지 않습니다."));

        // 댓글이 존재하는지 확인
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new AppException(ErrorCode.COMMENT_NOT_FOUND, "댓글이 존재하지 않습니다."));

        // 댓글 작성자와 삭제를 요청한 사용자의 userName이 일치하는지 확인
        // 일치하면 => 삭제
        // 일치하지 않으면 => error code
        if (user.getUserName() == comment.getUser().getUserName()) {
            commentRepository.delete(comment);

            CommentDeleteResponse commentDeleteResponse = CommentDeleteResponse.builder()
                    .message("댓글 삭제 완료")
                    .id(commentId)
                    .build();

            return commentDeleteResponse;

        } else {
            throw new AppException(ErrorCode.INVALID_PERMISSION, "본인이 작성한 댓글이 아니면 삭제할 수 없습니다.");
        }
    }


}
