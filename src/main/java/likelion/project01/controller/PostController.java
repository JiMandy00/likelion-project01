package likelion.project01.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import likelion.project01.domain.Response;
import likelion.project01.domain.dto.post.PostDeleteResponse;
import likelion.project01.domain.dto.post.*;
import likelion.project01.domain.entity.Post;
import likelion.project01.exception.AppException;
import likelion.project01.exception.ErrorCode;
import likelion.project01.service.LikeService;
import likelion.project01.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/posts")
@Api(tags = {"02. 게시글"})
public class PostController {

    private final PostService postService;
    private final LikeService likeService;

    // 조회
    @ApiOperation(value = "게시글 조회", notes = "전체 게시글 조회")
    @GetMapping("/list")
    public Response<PostListResponse> postList(Pageable pageable) {
        Page<Post> posts = postService.findAll(pageable);
        if (posts.getNumberOfElements() > 0) {
            return Response.success(new PostListResponse(posts));
        } else {
            throw new AppException(ErrorCode.POST_NOT_FOUND, "포스트가 작성되지 않았습니다.");
        }
    }

    // 작성
    @ApiOperation(value = "게시글 작성", notes = "title, content를 입력하여 게시글을 작성")
    @PostMapping("")
    public Response<PostCreateResponse> createPost(@ApiIgnore Authentication authentication, @RequestBody PostCreateRequest request) {
        String userName = authentication.getName();
        PostDto postDto = postService.createPost(request, userName);
        return Response.success(new PostCreateResponse(postDto.getId(), "글 작성이 완료 되었습니다."));
    }

    // 수정
    @ApiOperation(value = "게시글 수정", notes = "본인이 작성한 게시글 수정")
    @PutMapping("/{id}")
    public Response<PostEditResponse> editPost(Authentication authentication, @PathVariable("id") Long postId, @RequestBody PostUpdateRequest postUpdateRequest) {
        String userName = authentication.getName();
        Long returnPostId = postService.edit(postId, postUpdateRequest, userName);
        return Response.success(new PostEditResponse(returnPostId, "포스트 수정이 완료 되었습니다."));
    }

    // 삭제
    @ApiOperation(value = "게시글 삭제", notes = "본인이 작성한 게시글 삭제")
    @DeleteMapping("{id}")
    public Response<PostDeleteResponse> deletePost(Authentication authentication, @PathVariable(name = "id") Long postId) {
        String userName = authentication.getName();
        postService.delete(postId, userName);
        return Response.success(new PostDeleteResponse(postId, "포스트가 삭제 되었습니다."));
    }

    // 상세
    @ApiOperation(value = "게시글 상세 조회", notes = "특정 게시글 상세 조회")
    @GetMapping("/{postsId}")
    public Response<PostDetailResponse> detail(@PathVariable Long postId, Authentication authentication) {
        String userName = authentication.getName();
        PostDetailResponse postDetailResponse = postService.detail(userName, postId);
        return Response.success(new PostDetailResponse(
                postDetailResponse.getId(),
                postDetailResponse.getTitle(),
                postDetailResponse.getContent(),
                postDetailResponse.getUserName(),
                postDetailResponse.getCreateAt(),
                postDetailResponse.getLastModifiedAt()));
    }

}


