package likelion.project01.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import likelion.project01.domain.Response;
import likelion.project01.domain.dto.Alram.AlarmDto;
import likelion.project01.service.AlarmService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/alarms")
@Api(tags = {"05. 알람"})
public class AlarmController {
    private final AlarmService alarmService;


    @ApiOperation(value = "알람 조회", notes = "개인에게 온 알람을 조회")
    @GetMapping("")
    private Response<Page<AlarmDto>> alramList(@PageableDefault(size = 20, sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable, Authentication authentication) {
        String userName = authentication.getName();
        Page<AlarmDto> alramList = alarmService.alarmList(pageable, userName);
        return Response.success(alramList);
    }
}
