package likelion.project01.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import likelion.project01.domain.Response;
import likelion.project01.domain.dto.User.*;
import likelion.project01.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
@Slf4j
@Api(tags = {"01. 회원가입/로그인"})
public class UserController {

    private final UserService userService;

    @ApiOperation(value = "회원가입", notes = "아이디, 비밀번호를 입력하여 회원가입 해봅시다.")
    @PostMapping("/join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest request) {
        log.info("회원가입 요청이 들어왔습니다 = {}", request.toString());
        UserDto userDto = userService.join(request);
        return Response.success(new UserJoinResponse(userDto.getId(), userDto.getUserName()));
    }

    @ApiOperation(value = "로그인", notes = "아이디, 비밀번호를 입력하여 로그인 해봅시다.")
    @PostMapping("login")
    public Response<Object> log(@RequestBody UserLoginRequest request) {
        log.info("로그인 요청이 들어왔습니다 = {}", request.toString());
        String token = userService.login(request.getUserName(), request.getPassword());
        return Response.success(new UserLoginResponse(token));
    }
}
