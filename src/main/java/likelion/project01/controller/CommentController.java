package likelion.project01.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import likelion.project01.domain.Response;
import likelion.project01.domain.dto.comment.*;
import likelion.project01.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/posts")
@Api(tags = {"03. 댓글"})
public class CommentController {

    private final CommentService commentservice;

    // 1. 댓글 목록 조회
    @ApiOperation(value = "댓글 목록 조회", notes = "해당 게시글의 댓글 목록")
    @GetMapping("/{postId}/comments")
    public Response<CommentListResponse> commentList(@PathVariable Long postId, @PageableDefault(size = 10, sort = "createdAt",direction = Sort.Direction.DESC) Pageable pageable) {
        CommentListResponse commentListResponse = commentservice.commentList(postId, pageable);
        return Response.success(commentListResponse);
    }

    // 2. 댓글 작성
    @ApiOperation(value = "댓글 작성", notes = "해당 게시글에 댓글 작성")
    @PostMapping("/{postId}/comments")
    public Response<CommentDto> createComment(Authentication authentication, @PathVariable Long postId, @RequestBody CommentCreateRequest commentCreateRequest) {
        String userName = authentication.getName();
        CommentDto commentDto = commentservice.createComment(userName, postId, commentCreateRequest);
        return Response.success(commentDto);
    }

    // 3. 댓글 수정
    @ApiOperation(value = "댓글 수정", notes = "본인이 작성한 댓글 수정")
    @PutMapping("/{postId}/comments/{id}")
    public Response<CommentUpdateResponse> editComment(Authentication authentication, @PathVariable Long postId, @PathVariable(name = "id") Long commentId, @RequestBody CommentUpdatetRequest commentUpdatetRequest) {
        String userName = authentication.getName();
        CommentUpdateResponse commentUpdateResponse = commentservice.editComment(commentUpdatetRequest, postId, commentId, userName);
        return Response.success(commentUpdateResponse);
    }

    // 4. 댓글 삭제
    @ApiOperation(value = "댓글 삭제", notes = "본인이 작성한 댓글 삭제")
    @DeleteMapping("{postId}/comments/{id}")
    public Response<CommentDeleteResponse> deleteComment(Authentication authentication, @PathVariable Long postId, @PathVariable(name = "id") Long commentId) {
        String userName = authentication.getName();
        CommentDeleteResponse commentDeleteResponse = commentservice.deleteComment(userName, commentId, postId);
        return Response.success(commentDeleteResponse);
    }
}
