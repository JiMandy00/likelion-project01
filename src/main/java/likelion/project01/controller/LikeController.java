package likelion.project01.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import likelion.project01.domain.Response;
import likelion.project01.domain.dto.like.LikeResponse;
import likelion.project01.service.LikeService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/posts")
@Api(tags = {"04. 좋아요"})
public class LikeController {

    private final LikeService likeService;

    @ApiOperation(value = "좋아요", notes = "클릭하면 좋아요가 눌러집니다.")
    @PostMapping("/{postId}/likes")
    public Response<LikeResponse> putLike(@PathVariable Long postId, Authentication authentication) {
        String userName = authentication.getName();
        String message = likeService.putLike(userName, postId);
        LikeResponse likeResponse = new LikeResponse(message);
        return Response.success(new LikeResponse(likeResponse.getMessage()));
    }

    @ApiOperation(value = "좋아요 취소", notes = "클릭하면 눌러진 좋아요가 취소됩니다.")
    @GetMapping("/{postId}/likes")
    public Response<Integer> countLike(@PathVariable Long postId) {
        int cnt = likeService.countLike(postId);
        return Response.success(cnt);
    }
}
