package likelion.project01.controller;

import likelion.project01.domain.Response;
import likelion.project01.domain.dto.hello.HelloResponse;
import likelion.project01.service.HelloService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/hello")
@RequiredArgsConstructor
public class HelloController {

    private final HelloService helloService;

    @GetMapping
    public ResponseEntity<String> hello() {
//        return "안뇽안뇽 자동배포 되니? 되길 바란다 ^.^";
//        return ResponseEntity.ok().body("popin");
//        return ResponseEntity.ok().body("happy_new_year");
        return ResponseEntity.ok().body("jihwan");
    }

    @GetMapping("/{num}")
    public Response<HelloResponse> sum(@PathVariable int num) {
        return Response.success(new HelloResponse(helloService.col(num)));
    }
}
