package likelion.project01.repository;

import likelion.project01.domain.entity.Likes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LikeRepository extends JpaRepository<Likes, Long> {
    String findByUserAndPost(String userName, Long postId);
}
