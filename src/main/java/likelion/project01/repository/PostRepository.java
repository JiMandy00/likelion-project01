package likelion.project01.repository;

import likelion.project01.domain.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PostRepository extends JpaRepository<Post, Long> {
//    Optional findById(Long id);
}
