package likelion.project01.repository;

import likelion.project01.domain.My;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MyRepository extends JpaRepository<My, Long> {
}
