package likelion.project01.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public class JwtUtil {

    @Value("$jwt.token.secret}")
    private String secretKey;

    // token 열기
    public static Claims openToken(String token, String key) {
        // token과 token key를 return
        return Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody();
    }

    // 만료 여부를 확인
    public static boolean isExpired(String token, String secretKey) {
        return openToken(token, secretKey)
                .getExpiration()
                .before(new Date());
    }

    public static String getUserName(String token, String key) {
        return Jwts.parser().setSigningKey(key).parseClaimsJws(token)
                .getBody()
                .get("userName",String.class);
    }

    // token 생성
    public static String createToken(String userName, String key, long expireTime){
        Claims claims = Jwts.claims();
        claims.put("userName", userName);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expireTime)) // token 만료 시간
                .signWith(SignatureAlgorithm.HS256, key)
                .compact();
    }
}
